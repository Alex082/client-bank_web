import React from "react";
import { List, ListItem, ListItemText } from "@mui/material";

const UserList = ({ users, onUserClick }) => {
  return (
    <div>
      <h2>Доступные пользователи</h2>
      <List>
        {users.map((user) => (
          <ListItem key={user.id} onClick={() => onUserClick(user.id)}>
            <ListItemText primary={user.name} />
          </ListItem>
        ))}
      </List>
    </div>
  );
};

export default UserList;
