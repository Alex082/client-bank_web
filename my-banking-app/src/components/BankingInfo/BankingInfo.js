import React, { useState, useEffect } from "react";
// import UserList from "../UserList/UserList";
import AccountList from "../AccountList/AccountList";

const BankingInfo = ({ userId }) => {
  const [user, setUser] = useState(null);
  const [userAccounts, setUserAccounts] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:9000/customers/${userId}`)
      .then((response) => response.json())
      .then((data) => setUser(data))
      .catch((error) => console.error("Error fetching user:", error));

    fetch(`http://localhost:9000/accounts?userId=${userId}`)
      .then((response) => response.json())
      .then((data) => setUserAccounts(data))
      .catch((error) => console.error("Error fetching accounts:", error));
  }, [userId]);

  return (
    <div>
      {user && (
        <div>
          <h2>Информация о пользователе:</h2>
          <p>ID пользователя: {user.id}</p>
          <p>Имя: {user.name}</p>
          <p>Email: {user.email}</p>
          <p>Возраст: {user.age}</p>
        </div>
      )}
      <h3>Счета пользователя:</h3>
      <AccountList userAccounts={userAccounts} />
    </div>
  );
};

export default BankingInfo;
