import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";

const AccountList = ({ userAccounts }) => {
  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>ID счета</TableCell>
            <TableCell>Номер счета</TableCell>
            <TableCell>Валюта</TableCell>
            <TableCell>Баланс</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {userAccounts.map((account) => (
            <TableRow key={account.id}>
              <TableCell>{account.id}</TableCell>
              <TableCell>{account.number}</TableCell>
              <TableCell>{account.currency}</TableCell>
              <TableCell>{account.balance}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default AccountList;
