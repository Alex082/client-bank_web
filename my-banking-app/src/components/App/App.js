import React, { useState, useEffect } from "react";
import UserList from "../UserList/UserList";
import BankingInfo from "../BankingInfo/BankingInfo";

const App = () => {
  const [users, setUsers] = useState([]);
  const [selectedUserId, setSelectedUserId] = useState(null);

  useEffect(() => {
    fetch("http://localhost:9000/customers")
      .then((response) => response.json())
      .then((data) => {
        setUsers(data);
        //первого пользователь по умолчанию при загрузке
        if (data.length > 0) {
          setSelectedUserId(data[0].id);
        }
      })
      .catch((error) => console.error("Error fetching users:", error));
  }, []);

  const handleUserClick = (userId) => {
    setSelectedUserId(userId);
  };

  return (
    <div>
      <h1>Banking App</h1>
      <UserList users={users} onUserClick={handleUserClick} />
      {selectedUserId && <BankingInfo userId={selectedUserId} />}
    </div>
  );
};

export default App;
